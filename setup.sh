pacman -S yay

yay -S base-devel

yay -S google-chrome

yay -S neovim

yay -S z-git
# yay -S zinit-git
# zinit light agkozak/zsh-z
# zinit ice depth=1; zinit light romkatv/powerlevel10k
# zinit light zsh-users/zsh-autosuggestions
# zinit light zsh-users/zsh-history-substring-search
# zinit light zsh-users/zsh-syntax-highlighting

yay -S lando-bin
sudo systemctl enable docker.service
sudo systemctl enable docker.socket
sudo usermod -aG docker $USER

yay -S asdf-vm
# . /opt/asdf-vm/asdf.sh
asdf plugin add ruby
asdf plugin-update ruby
asdf install ruby latest
asdf global ruby latest
